import Avatar from '../../components/avatar/Avatar';
import Button from '../../components/button/Button';
import SmallCard from '../../components/card-sm/SmallCard';
import Card from '../../components/card/Card';

const Home = () => {
  const addComment = () => {};
  return (
    <div className='flex'>
      <div className='w-4/6 overflow-y-auto scrollbar-hide'>
        {/* Story */}
        <div className='py-2 border border-x-0 border-t-0 border-b-2 flex max-w-full overflow-x-auto scrollbar-hide'>
          <div className='px-2'>
            <Avatar
              customClass={'border border-0 rounded-full'}
              height='64'
              width='64'
              source='https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
            />
            <p className='mt-2 w-16 text-center text-sm truncate'>Name</p>
          </div>
        </div>
        {/* Timeline */}
        <div>
          <Card
            name='Billie Eillish'
            location='Pittsburg, PL'
            source='https://i.pinimg.com/236x/f6/16/d0/f616d044c7937dde2e8ec2b4f3e6f79c--france-travel-winding-road.jpg'
          />
          <Card
            name='Billie Eillish'
            location='Pittsburg, PL'
            source='https://i.pinimg.com/236x/f6/16/d0/f616d044c7937dde2e8ec2b4f3e6f79c--france-travel-winding-road.jpg'
          />
          <Card
            name='Billie Eillish'
            location='Pittsburg, PL'
            source='https://i.pinimg.com/236x/f6/16/d0/f616d044c7937dde2e8ec2b4f3e6f79c--france-travel-winding-road.jpg'
          />
          <Card
            name='Billie Eillish'
            location='Pittsburg, PL'
            source='https://i.pinimg.com/236x/f6/16/d0/f616d044c7937dde2e8ec2b4f3e6f79c--france-travel-winding-road.jpg'
          />
        </div>
      </div>

      {/* Sidebar */}
      <div className='w-2/6 py-2 px-4 bg-slate-50'>
        <div className='flex w-full max-w-full'>
          <Avatar
            customClass={'border border-0 rounded-full'}
            height='64'
            width='64'
            source='https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
          />
          <div className='flex flex-1 w-1 justify-center flex-col text-sm px-4'>
            <p className='font-semibold truncate'>Subhaneet</p>
            <p className='font-extralight truncate'>Subhaneet Shrestha</p>
          </div>

          <div className='flex justify-center'>
            <Button
              customClass='capitalize text-xs text-sky-600'
              onClick={addComment}
              isDisabled={false}
              type='button'
              content='switch'
            />
          </div>
        </div>
        {/* Suggestions */}
        <div className='pt-6'>
          <div className='flex justify-between'>
            <p className='text-gray-400 text-sm font-semibold capitalize'>
              suggestions for you
            </p>

            <Button
              customClass='capitalize text-xs font-semibold'
              onClick={addComment}
              isDisabled={false}
              type='button'
              content='see all'
            />
          </div>

          <div className='py-3'>
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
            <SmallCard />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
