import './App.css';
import Navbar from './components/navbar/Navbar';
import Home from './pages/home/Home';

function App() {
  return (
    <div className='App bg-slate-50'>
      <Navbar />
      <div className='mt-8 mx-auto w-1/2 bg-white'>
        <Home />
      </div>
    </div>
  );
}

export default App;
