import { Bookmark, Heart, MessageCircle, Send, Smile } from 'react-feather';
import Avatar from '../avatar/Avatar';
import Button from '../button/Button';
import Input from '../form/Input';

const Card = ({...props}) => {
  const { customClass, name, location, source } = props;
  const addComment = () => {
    
  }
  return (
    <div className={`border-2 my-6 ${customClass ?? ''}`}>
      <div className='flex px-5 py-5'>
        <div>
        <Avatar customClass='border rounded-full' height='38' width='38' source='https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png' />
        </div>
        <div className='px-3 flex flex-col items-center'>
          <p className='font-bold text-sm'>{ name }</p>
          { location ? (<p className='font-thin text-xs text-opacity-70'>{ location }</p>) : null }
        </div>
      </div>

      <div className='w-full h-[38rem]'>
        <div className="bg-[url('https://i.pinimg.com/236x/f6/16/d0/f616d044c7937dde2e8ec2b4f3e6f79c--france-travel-winding-road.jpg')] bg-no-repeat bg-center h-full before:content-none"></div>
      </div>

      <div className='flex justify-between pt-3 px-6'>
        <div className='flex justify-between w-[6rem]'>
          <Heart />
          <MessageCircle />
          <Send />
        </div>

        <div>
          <Bookmark />
        </div>
      </div>

      <div className='text-sm px-6 py-3'>
        <p className='font-semibold'>123,333,444 likes</p>

        <div className='py-1 flex'>
          <p className='font-semibold'>Billie EIilish</p>
          <p className='pl-1'>last night</p>
        </div>

        <div className='opacity-60'>
          <a className='no-underline'>View 444,555 comments</a>
          <p className='text-xs uppercase'>8 hours ago</p>
        </div>
      </div>

      <div className='px-6 py-3'>
        <form className='flex'>
          <Smile />
          <Input customClass='border-0 w-11/12 mx-2 outline-none' type='text' placeholder='Add a comment' required={true} isDisabled={false} />
          <Button customClass='text-sky-400 capitalize' type='submit' onClick={addComment} isDisabled={false} content='post' />
        </form>
      </div>
    </div>
  );
}
 
export default Card;