import Avatar from '../avatar/Avatar';
import Button from '../button/Button';

const SmallCard = ({ ...props }) => {
  const { source, name, bottomText, buttonClick } = props;
  const buttonClicked = () => {};
  return (
    <div className='flex w-full py-2 max-w-full'>
      <Avatar
        customClass={'border border-0 rounded-full'}
        height='38'
        width='38'
        source='https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
      />
      <div className='flex flex-1 w-1 justify-center flex-col text-sm px-4'>
        <p className='font-semibold truncate'>Subhaneet</p>
        <p className='font-extralight text-xs truncate'>Subhaneet Shrestha</p>
      </div>

      <div className='flex justify-center'>
        <Button
          customClass='capitalize text-xs text-sky-600'
          onClick={buttonClicked}
          isDisabled={false}
          type='button'
          content='follow'
        />
      </div>
    </div>
  );
};

export default SmallCard;
