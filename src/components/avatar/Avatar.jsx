const Avatar = ({...props}) => {
  const { source, onError, height, width, customClass } = props;
  return (
    <img className={customClass} src={source} height={height} width={width} onError={onError} />
  );
}
 
export default Avatar;