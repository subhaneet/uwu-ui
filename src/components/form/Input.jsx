const Input = ({...props}) => {
  const { customClass, type, placeholder, onChange, required, isDisabled } = props;
  return (
    <input className={customClass} type={type} placeholder={placeholder} onChange={(e) => onChange(e.target.value)} required={required} disabled={isDisabled} />
  );
}
 
export default Input;