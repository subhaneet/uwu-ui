const Button = ({...props}) => {
  const { customClass, content, onClick, type, isDisabled } = props;
  return (
    <button type={type} disabled={isDisabled} onClick={onClick} className={customClass}>{ content }</button>
  );
}
 
export default Button;