import { useState } from 'react';
import { Home, PlusSquare, Compass, Heart, MessageSquare, Search } from 'react-feather';
import Avatar from '../avatar/Avatar';
import Input from '../form/Input';

const Navbar = () => {
  const [searchText, setSearchText] = useState('');
  const search = (text) => {
    setSearchText(text);
  }

  const followClicked = () => {
    console.log('here')
  }

  return (
    <div className='w-full bg-white'>
      <div className='flex justify-between sticky w-1/2 mx-auto py-3 border border-x-0 border-b-gray-200'>
        <div className='flex items-center w-3/12 text-center'>
          <h6 className='pl-2 text-2xl logo-font'>
            uwu
          </h6>
        </div>

        <div className='flex justify-end flex-1'>
          <div className='flex-1 flex justify-end'>
            <div className='flex min-w-40'>
              <div className={'absolute p-2.5'}>
                <Search color='gray' opacity={'60%'} size={20} />
              </div>
              <Input customClass={'border-2 rounded-md pl-8 h-9 bg-gray-100 mr-12'} placeholder='Search' type='search' onChange={search} />
            </div>
          </div>

          <div className='flex items-center justify-evenly w-1/3 '>
            <Home cursor={'pointer'} />
            <MessageSquare cursor={'pointer'} textAnchor='20' />
            <PlusSquare cursor={'pointer'} />
            <Compass cursor={'pointer'} />
            <Heart cursor={'pointer'} onClick={followClicked} fill={'white'} />
            <Avatar customClass='border border-0 rounded-full cursor-pointer' height='24' width='24' source='https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png' />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Navbar;